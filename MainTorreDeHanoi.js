const TorreDeHanoi = {
    startGameClickElements: function () {
        const gamesection = document.getElementById('game_section');
        gamesection.addEventListener('click', this.comunicationMethods)
    },

    comunicationMethods: function (event) {

        if (Click.isDisc(event)) {
            Click.getDisc(event)
            Message.selectDisc()

        } else if (Click.isStick(event) && discSelected !== null) {
            Click.getStick(event)
            Movement.isLastChild = DiscSelected.isLastChild(discParent)
            Movement.haveLessWidth = DiscSelected.haveLessWidth(event)
            Movement.moveDisc()
        }
    }
}

TorreDeHanoi.startGameClickElements()