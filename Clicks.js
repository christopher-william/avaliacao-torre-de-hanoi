let discSelected = null, stickSelected = null, discParent = null
const Click = {

    isDisc: (e) => {
        const idDiscs = ['disc1', 'disc2', 'disc3', 'disc4']

        if (idDiscs.includes(e.target.id)) {
            return true
        } else {
            return false
        }
    },

    isStick: (e) => {

        if (e.target.className.includes('stick')) {
            return true
        } else {
            return false
        }
    },

    getDisc: function (e) {
        discParent = e
        discSelected = e.target
    },

    getStick: function (e) {
        stickSelected = e.target
    }

}