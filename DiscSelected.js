const DiscSelected = {

    isLastChild: (e) => {

        if (e.path[1].lastElementChild === null) {
            return false
        } else if (e.path[1].lastElementChild.id === e.target.id) {
            return true
        } else {
            return false
        }
    },

    haveLessWidth: (e) => {

        if (e.target.lastElementChild === null) {
            return true
        } else if (e.target.lastElementChild.clientWidth > discSelected.clientWidth) {
            return true
        } else {
            return false
        }
    }
}