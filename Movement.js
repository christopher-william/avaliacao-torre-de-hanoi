const Movement = {

    isLastChild: false,
    haveLessWidth: false,

    moveDisc: function () {

        if (this.isLastChild && this.haveLessWidth) {
            if (Message.discMoves() === 1) { Timer.start() }

            this.moveSong()
            stickSelected.appendChild(discSelected)
            stickSelected = null; discSelected = null;
            Message.winGame()
        }
    },

    moveSong: function () {
        const placed = new Audio("./sounds/place.wav");
        placed.play();
    }
}