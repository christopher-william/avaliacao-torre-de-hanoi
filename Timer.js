let intervaloMilezimos;
let minutes = 0, seconds = 0
const Timer = {

    start: () => {
        const timer = document.getElementById('timer')
        timer.innerHTML = 'Time: 00:00'

        intervaloMilezimos = setInterval(() => {
            seconds += 1

            if (seconds === 60) {
                minutes += 1
                seconds = 0
            }

            if (seconds < 10 && minutes < 10) {
                timer.innerHTML = 'Time: 0' + minutes + ':0' + seconds
            } else if (seconds < 10 && minutes > 10) {
                timer.innerHTML = 'Time: 0' + minutes + ':0' + seconds
            } else if (seconds > 10 && minutes < 10) {
                timer.innerHTML = 'Time: 0' + minutes + ':' + seconds
            } else if (seconds === 10) {
                timer.innerHTML = 'Time: 0' + minutes + ':' + seconds
            } else {
                timer.innerHTML = 'Time: ' + minutes + ':' + seconds
            }
        }, 1000)

    },

    stop: () => {
        clearInterval(intervaloMilezimos)
        minutes = 0
        seconds = 0
    }
}
